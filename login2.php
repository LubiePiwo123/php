<?php 
 
if (!empty($_POST['log'])) 
{ 
if (!empty($_POST['pass'])) 
{ 
if (($_POST['log'] == "Janek") && ($_POST['pass'] == "zaq12wsx")) 
{ 
print "<h2>Zalogowałeś się prawidłowo<br><br>Witamy w serwisie!</h2>"; 
}else die ("Nie ma takiego użytkownika lub hasło jakie wprowadziłeś jest nieprawidłowe<br><br>Prosimy spróbuj ponownie."); 
 
}else die ("Nie wpisałeś hasła!"); 
 
}else die ("Żeby wejść do serwisu musisz wpisać swój login a póżniej hasło!"); 
 
?>
<!doctype html>
<html lang="pl">
<head>
<meta charset="utf-8"/>
<title>zalogowany</title>

</head>
<body>
<div>
<h2>Formularz logowania z zapytaniem</h2>
<img src="net1.png"/><br><img src="net2.png"/><br><img src="net3.png"/>
</div>
<hr>
<div>
<a href="https://html.spec.whatwg.org/multipage/">dokumentacja HTML5</a>

</div>
<div>
<h1>Co to jest HTML5</h1>
<h2><u>Fragmenty dokumentacji HTML5</u></h2>
<h3>Wprowadzenie</h3>
<p>
Termin <b>„HTML5” </b>jest powszechnie używany jako modne słowo w odniesieniu do nowoczesnych technologii internetowych, z których wiele (choć bynajmniej nie wszystkie) zostało opracowanych przez WHATWG. Ten dokument jest jednym z takich; inne są dostępne w przeglądzie standardów WHATWG. 
</p>
<p>
HTML jest podstawowym językiem znaczników w sieci WWW. Pierwotnie HTML został zaprojektowany przede wszystkim jako język do semantycznego opisu dokumentów naukowych. Jego ogólna konstrukcja pozwoliła jednak na jego dostosowanie w kolejnych latach do opisywania wielu innych rodzajów dokumentów, a nawet aplikacji. 
</p>
<p>
W szczególności znajomość podstaw <b>DOM </b>jest niezbędna do pełnego zrozumienia niektórych bardziej technicznych części tej specyfikacji. Zrozumienie <b>Web IDL, HTTP, XML, Unicode, kodowania znaków, JavaScript i CSS </b>będzie również pomocne w niektórych miejscach.
</p>
<p>
Przez pierwsze pięć lat (1990-1995) HTML przeszedł szereg poprawek i doświadczył wielu rozszerzeń, głównie hostowanych najpierw w CERN, a następnie w IETF.

Wraz z utworzeniem W3C, rozwój HTML ponownie zmienił miejsce. Pierwsza nieudana próba rozszerzenia języka HTML w 1995 roku, znana jako HTML 3.0, ustąpiła miejsca bardziej pragmatycznemu podejściu znanemu jako HTML 3.2, które zostało ukończone w 1997 roku. HTML4 szybko pojawił się w tym samym roku.
</p>
<p>
W następnym roku członkowie W3C postanowili zaprzestać ewolucji HTML i zamiast tego rozpocząć pracę nad odpowiednikiem opartym na XML, zwanym XHTML. Wysiłki te rozpoczęły się od przeformułowania HTML4 w XML, znanego jako XHTML 1.0, który nie dodał żadnych nowych funkcji poza nową serializacją i który został ukończony w 2000 roku. Po XHTML 1.0, W3C skupiło się na ułatwieniu innym grupom roboczym extend XHTML, pod szyldem Modularization XHTML. Równolegle W3C pracowało również nad nowym językiem, który nie był kompatybilny z wcześniejszymi językami HTML i XHTML, nazywając go XHTML2.
</p>
<p>
Mniej więcej w czasie, gdy ewolucja HTML została zatrzymana w 1998 roku, części API dla HTML opracowane przez dostawców przeglądarek zostały określone i opublikowane pod nazwami DOM Level 1 (w 1998) oraz DOM Level 2 Core i DOM Level 2 HTML (od 2000 i z kulminacją w 2003 r.). Wysiłki te następnie wygasły, a niektóre specyfikacje DOM dla poziomu 3 zostały opublikowane w 2004 r., ale grupa robocza została zamknięta przed ukończeniem wszystkich wersji roboczych poziomu 3.
</p>

<p>
W 2003 roku opublikowanie XForms, technologii pozycjonowanej jako następna generacja formularzy internetowych, wywołało ponowne zainteresowanie ewolucją samego kodu HTML, zamiast szukania dla niego zamienników. Zainteresowanie to wzięło się z uświadomienia sobie, że wdrożenie XML jako technologii internetowej było ograniczone do całkowicie nowych technologii (takich jak RSS i później Atom), a nie jako zamiennika istniejących wdrożonych technologii (takich jak HTML).
</p>
<p>
Pierwszym rezultatem tego odnowionego zainteresowania był dowód koncepcji pokazujący, że możliwe było rozszerzenie formularzy HTML4, aby zapewnić wiele funkcji wprowadzonych przez XForms 1.0, bez wymagania od przeglądarek implementacji silników renderujących, które były niezgodne z istniejącymi stronami internetowymi HTML. Na tym wczesnym etapie, podczas gdy szkic był już publicznie dostępny, a informacje były już zbierane ze wszystkich źródeł, specyfikacja podlegała wyłącznie prawu autorskiemu Opera Software.
</p>
<p>
Pomysł, że ewolucja HTML powinna zostać ponownie otwarta, został przetestowany na warsztatach W3C w 2004 roku, gdzie niektóre z zasad leżących u podstaw pracy HTML5 (opisane poniżej), a także wspomniana wcześniej propozycja wstępna obejmująca tylko funkcje związane z formularzami, zostały zaprezentowane W3C wspólnie przez Mozillę i Operę. Propozycja została odrzucona, ponieważ była sprzeczna z wcześniej wybranym kierunkiem rozwoju sieci; pracownicy i członkowie W3C głosowali za kontynuacją opracowywania zamienników opartych na XML.
</p>
<p>
Wkrótce potem Apple, Mozilla i Opera wspólnie ogłosiły zamiar kontynuowania prac pod parasolem nowego miejsca o nazwie WHATWG. Utworzono publiczną listę mailingową, a wersja robocza została przeniesiona na stronę WHATWG. Prawa autorskie zostały następnie zmienione, aby były wspólną własnością wszystkich trzech dostawców i aby umożliwić ponowne wykorzystanie specyfikacji.
</p>
<p>
WHATWG opierało się na kilku podstawowych zasadach, w szczególności o tym, że technologie muszą być kompatybilne wstecz, że specyfikacje i implementacje muszą być zgodne, nawet jeśli oznacza to zmianę specyfikacji, a nie implementacji, oraz że specyfikacje muszą być wystarczająco szczegółowe, aby implementacje mogły osiągnąć pełna interoperacyjność bez wzajemnej inżynierii wstecznej.
</p>
<p>
Ten ostatni wymóg w szczególności wymagał, aby zakres specyfikacji HTML5 obejmował to, co zostało wcześniej określone w trzech oddzielnych dokumentach: HTML4, XHTML1 i DOM2 HTML. Oznaczało to również uwzględnienie znacznie większej liczby szczegółów, niż wcześniej uważano za normę.
</p>
<p>
W 2006 roku W3C wyraziło mimo wszystko zainteresowanie udziałem w rozwoju HTML5, aw 2007 roku utworzyło grupę roboczą powołaną do pracy z WHATWG nad rozwojem specyfikacji HTML5. Apple, Mozilla i Opera pozwoliły W3C na opublikowanie specyfikacji w ramach praw autorskich W3C, zachowując wersję z mniej restrykcyjną licencją na stronie WHATWG.
</p>
<p>
Przez kilka lat obie grupy pracowały razem. Jednak w 2011 roku grupy doszły do ​​wniosku, że mają różne cele: W3C chciało opublikować „skończoną” wersję „HTML5”, podczas gdy WHATWG chciało kontynuować prace nad Living Standard dla HTML, stale utrzymując specyfikację zamiast zamrażać go w stanie ze znanymi problemami i dodawać nowe funkcje w razie potrzeby, aby ewoluować platformę.
</p>
<p>
W 2019 roku WHATWG i W3C podpisały umowę o współpracy nad jedną wersją HTML w przyszłośc.
</p>
</div>
<div>
<h1>Rozciągliwość</h1>

<p>
HTML ma szeroką gamę mechanizmów rozszerzalności, które można wykorzystać do dodawania semantyki w bezpieczny sposób:
</p>
<p>
Autorzy mogą używać atrybutu <b>class</b> do rozszerzania elementów, skutecznie tworząc własne elementy, jednocześnie używając najbardziej odpowiedniego istniejącego „prawdziwego” elementu HTML, dzięki czemu przeglądarki i inne narzędzia, które nie znają rozszerzenia, nadal mogą je dość dobrze obsługiwać. Taką technikę stosują na przykład mikroformaty.
</p>
<p>
Autorzy mogą dołączyć dane dla wbudowanych skryptów po stronie klienta lub skryptów po stronie serwera w całej witrynie do przetworzenia przy użyciu atrybutów <b>data-*=""</b>. Gwarantują one, że nigdy nie zostaną one dotknięte przez przeglądarki i pozwalają skryptom na umieszczanie danych w elementach HTML, które skrypty mogą następnie wyszukiwać i przetwarzać.
</p>
<p>
Autorzy mogą używać mechanizmu<b> <meta name="" content=""> </b>w celu dołączania metadanych dotyczących całej strony.
</p>
<p>
Autorzy mogą używać mechanizmu <b>rel=""</b> do adnotowania linków o określonych znaczeniach, rejestrując rozszerzenia do wstępnie zdefiniowanego zestawu typów linków. Jest to również używane przez mikroformaty.
</p>
<p>
Autorzy mogą osadzić nieprzetworzone dane za pomocą mechanizmu <b><script type=""> </b>z niestandardowym typem, w celu dalszej obsługi przez skrypty wbudowane lub po stronie serwera.
</p>
<p>
Autorzy mogą rozszerzać <b>API</b> za pomocą mechanizmu prototypowania JavaScript. Jest to powszechnie używane na przykład przez biblioteki skryptów.
</p>
<p>
Autorzy mogą używać funkcji mikrodanych <b>(atrybuty itemscope="" i itemprop="")</b> do osadzania zagnieżdżonych par danych nazwa-wartość, które mają być udostępniane innym aplikacjom i witrynom.
</p>
</div>


</body>
</html>
